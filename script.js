const botaoConsultar = document.querySelector("#consultar");
const botaoLimpar = document.querySelector("#limpar");
const consulta = document.querySelectorAll("#consulta input");


let cep = document.querySelector("#cep");

function extrairJSON(resposta){
    return resposta.json();
}

function iniciarConsulta(){
    fetch(`https://viacep.com.br/ws/${cep.value}/json/`).then(extrairJSON).then(preencherFormulario)
}

function preencherFormulario(dados){
    consulta[0].value = dados.logradouro;
    consulta[1].value= dados.bairro;
    consulta[2].value = dados.localidade;
    consulta[3].value = dados.uf;
}

function limparCampos(){
    cep.value = "";
    consulta[0].value = "";
    consulta[1].value= "";
    consulta[2].value = "";
    consulta[3].value = "";
}

botaoConsultar.onclick = iniciarConsulta;
botaoLimpar.onclick = limparCampos;